#include "bmp_transformer.h"
#include "fileIO.h"
#include "image_transformer.h"
#include <time.h>
#include <string.h>

int main(int argc, char **argv) {
    (void) argc;
    (void) argv; // suppress 'unused parameters' warning

    printf("image_transformer SOURCE DIST [FLAG] \n");
    printf("FLAGS: \n");
    printf("asm: run with SSE optimization \n\n");

    if (argc < 3) {
        fprintf(stderr, "image_transformer takes 2 or 3 parameters.\n");
        return 0;
    }

    FILE *input = input_file(argv[1], "r");
    if (input == NULL) {
        fprintf(stderr, "Cannot open source file. \n");
        return -1;
    }

    FILE *output = input_file(argv[2], "w");
    if (output == NULL) {
        fprintf(stderr, "Cannot open target file. \n");
        return -1;
    }

    struct image image = {0};
    enum read_status read_status = from_bmp(input, &image);
    fclose(input);
    if (read_status == READ_OK) {
        clock_t start, end;
        start = clock();
        if (argc == 4 && strcmp(argv[3], "asm") == 0) {
            sepia_simd(image);
        } else {
            sepia(image);
        }
        end = clock();
        double time_taken = ((double)(end - start))/CLOCKS_PER_SEC;
        printf( "Time elapsed in microseconds: %f\n", time_taken );
        enum write_status write_status = to_bmp(output, &image);
        fclose(output);
        if (write_status == WRITE_OK) {
            fprintf(stdout, "Successfully performed operation \n");
            free(image.data);
        } else if (write_status == WRITE_ERROR) {
            fprintf(stderr, "Error when writing in file \n");
            free(image.data);
            return -1;
        }
        return 0;
    } else {
        if (read_status == READ_INVALID_HEADER) {
            fprintf(stderr, "Wrong image header passed \n");
        } else if (read_status == READ_INVALID_SIGNATURE) {
            fprintf(stderr, "Wrong signature passed \n");
        } else if (read_status == READ_INVALID_BITS) {
            fprintf(stderr, "Incorrect bits in file \n");
        }

        close_file(input);
        close_file(output);

        return -1;
    }

}
