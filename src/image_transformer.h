#pragma once
#include "image.h"
#include <stdlib.h>

struct image rotate( struct image const source );
void sepia( struct image const source );
extern void sepia_simd(struct image image);
