#include "fileIO.h"

FILE* input_file(const char *filename, const char *mode) {
    FILE *input;
    if ((input = fopen(filename, mode)) == NULL) {
        return NULL;
    }
    return input;
}

void close_file(FILE* file) {
    fclose(file);
}
