#include "image_transformer.h"
#include <stdio.h>

#define PIXEL_SIZE 3
#define SEPIA_BASE 20

static uint32_t address_from_coordinates(uint32_t row, uint32_t column, uint32_t width) {
    return row * width + column;
}

static uint8_t normalize_color(int64_t value) {
    if (value > UINT8_MAX)
        value = 255;
    if (value < 0)
        value = 0;
    return (uint8_t) value;
}


struct image rotate(struct image const source) {
    struct image rotated_image = {0};
    int32_t width = (int32_t) source.width;
    int32_t height = (int32_t) source.height;
    rotated_image.data = (struct pixel *) malloc(width * height * PIXEL_SIZE);
    if (rotated_image.data == NULL) {
        fprintf(stderr, "Out of memory \n");
        abort();
    }
    rotated_image.height = width;
    rotated_image.width = height;
    for (int32_t column = 0; column < width; column++) {
        for (int32_t row = height - 1; row >= 0; row--) {
            rotated_image.data[column * height + (height - 1 - row)] = source.data[address_from_coordinates(row, column, width)];
        }
    }
    return rotated_image;
}

static void sepia_one( struct pixel* const pixel ) {
    static const float c[3][3] = {
    { .393f, .769f, .189f },
    { .349f, .686f, .168f },
    { .272f, .543f, .131f }
    };
    struct pixel const old = *pixel;
    pixel->r = normalize_color(old.r * c[0][0] + old.g * c[0][1] + old.b * c[0][2]);
    pixel->g = normalize_color(old.r * c[1][0] + old.g * c[1][1] + old.b * c[1][2]);
    pixel->b = normalize_color(old.r * c[2][0] + old.g * c[2][1] + old.b * c[2][2]);
}

void sepia( struct image const source ) {
    for (int32_t column = 0; column < width; column++) {
        for (int32_t row = height - 1; row >= 0; row--) {
            sepia_one(&source.data[row * width + column]);
        }
    }
}
