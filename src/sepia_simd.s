section .data

c1 dd 0.189, 0.168, 0.131, 0.189
c3 dd 0.393, 0.349, 0.272, 0.393
c2 dd 0.769, 0.686, 0.534, 0.769
max_rgb: dd 255, 255, 255, 255

section .text
global	sepia_simd

sepia_simd:
	push rbp
    push rbx

    mov r8, [rsp + 24]
    imul r8, [rsp + 32]
    mov rax, [rsp + 40]

    mov r9, c1
    movaps xmm3, [r9]
    mov r9, c2
    movaps xmm4, [r9]
    mov r9, c3
    movaps xmm5, [r9]
    mov r9, max_rgb
    movdqa xmm6, [r9]

    mov r9, 0
    .loop:
    	cmp	r9, r8
    	jge .exit

        pxor xmm0, xmm0
        pxor xmm1, xmm1
        pxor xmm2, xmm2

        mov rdi, r9
        imul rdi, 3
        pinsrb xmm0, [rax + rdi], 0
        pinsrb xmm0, [rax + rdi], 4
        pinsrb xmm0, [rax + rdi], 8
        pinsrb xmm1, [rax + 1 + rdi], 0
        pinsrb xmm1, [rax + 1 + rdi], 4
        pinsrb xmm1, [rax + 1 + rdi], 8
        pinsrb xmm2, [rax + 2 + rdi], 0
        pinsrb xmm2, [rax + 2 + rdi], 4
        pinsrb xmm2, [rax + 2 + rdi], 8

        cvtdq2ps xmm0, xmm0
        cvtdq2ps xmm1, xmm1
        cvtdq2ps xmm2, xmm2

        mulps xmm0, xmm3
        mulps xmm1, xmm4
        mulps xmm2, xmm5
        addps xmm0, xmm1
        addps xmm0, xmm2

        cvtps2dq xmm0, xmm0
        pminud xmm0, xmm6

        pextrb [rax + rdi], xmm0, 8
        pextrb [rax + rdi + 1], xmm0, 4
        pextrb [rax + rdi + 2], xmm0, 0

        inc r9
        jmp	.loop

.exit:
    pop	rbx
    pop	rbp
    ret