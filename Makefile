CC=gcc
AS=nasm
CFLAGS=-O2
ASFLAGS=-felf64
LDFLAGS=-lm -z noexecstack

C_SOURCES=$(shell find src/ -type f -name '*.c')
C_OBJECTS=$(C_SOURCES:.c=.o)

ASM_SOURCES=$(shell find src/ -type f -name '*.s')
ASM_OBJECTS=$(ASM_SOURCES:.s=.o)

.PHONY: all clean
all: main

main: $(C_OBJECTS) $(ASM_OBJECTS)
	$(CC) $(LDFLAGS) $^ -o main
	
%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

%.o: %.s
	$(AS) $(ASFLAGS) $< -o $@

clean:
	rm -f $(C_OBJECTS) $(ASM_OBJECTS)